/**
 * Created by divby on 06.08.2015.
 */
$(function () {
    var $startButton = $('#start'),
        $stopButton = $('#stop'),
        $status = $('#status'),
        $inputUrl = $('#url'),
        basicSrc = null;

    $startButton.click(function (e) {
        e.preventDefault();

        if (!basicSrc) {
            basicSrc = $status.attr('src');
        }

        var url = $inputUrl.val();

        if (!url) {
            alert('Введите адрес сайта!')
        }

        $status.attr('src', basicSrc + '?url=' + url)

    });

    $stopButton.click(function (e) {
        e.preventDefault();
        $status.attr('src', basicSrc);
    });

});

