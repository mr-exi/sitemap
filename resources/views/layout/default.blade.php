<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    <link href="{{ elixir('css/app.css') }}" rel="stylesheet" type="text/css">

</head>
<body>
<div class="container">

    <div class="header">
        <div class="right">
            <a href="/">Построение карты</a>
            <a href="{{ action('Sitemap\SitemapController@index') }}">Архив</a>
        </div>
    </div>

    <div class="content">
        @yield('content')
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ elixir('js/app.js') }}"></script>
</body>
</html>
