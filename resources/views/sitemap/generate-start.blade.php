<head>
    <script type="text/javascript">
        function updateStatus(data) {
            if (!!data) {

                cupdate('current', data.current);
                cupdate('scanned', data.scanned);
                cupdate('added', data.added);
                cupdate('left', data.left);
                cupdate('tpassed', data.time.passedFormat);
                cupdate('tleft', data.time.leftFormat);

            }
        }

        function cupdate(id, txt) {
            var el = document.getElementById(id);
            el.innerHTML = txt;
        }
        function fupdate(txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8) {
            cupdate('cpage', txt1);
            cupdate('pleft', txt2);
            cupdate('pdone', txt3);
            cupdate('bdone', txt4);
            cupdate('tdone', txt5);
            cupdate('tleft', txt6);
            cupdate('llevel', txt7);
            cupdate('added', txt8);
        }

    </script>
</head>
<body>
<div id="status">
    Текущая страница: <span id="current"></span><br>
    Просканировано: <span id="scanned"></span><br>
    Добавлено: <span id="added"></span><br>
    Осталось: <span id="left"></span><br>
    Времени прошло: <span id="tpassed"></span><br>
    Времени осталось: <span id="tleft"></span><br>
</div>