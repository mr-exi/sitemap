<script>updateStatus({!! json_encode($data) !!});</script>
@if($data['complete'])
    <a href="{{$data['downloadUrl']}}" target="_blank">Скачать</a>
@elseif($data['wait'] && $data['redirect'])
    <script>location.href = "{!!  $data['redirect'] !!}"</script>
@endif