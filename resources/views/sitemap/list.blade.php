@extends('layout.default')

@section('content')
    <table class="sitemaps">
        <tr>
            <th>Проект</th>
            <th>Дата</th>
            <th>Скачать в формате xml</th>
        </tr>
        @foreach($items as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{ date('d.m.Y', strtotime($item->created_at))}}</td>
                <td><a href="{{action('Sitemap\SitemapController@getFile',['id' => $item->id])}}">Скачать</a></td>
            </tr>
        @endforeach
    </table>

    {!! $pagination !!}
@stop