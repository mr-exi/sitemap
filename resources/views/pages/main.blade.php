@extends('layout.default')

@section('content')
    <form>
        <label>
            <input id="url" type="text" value="" placeholder="Введите URL сайта">
            <br>
            <small>Например "http://site.ru/"</small>
        </label>


        <div class="row">
            <input id="start" type="submit" name="start" value="Начать">
            <input id="stop" type="submit" name="stop" value="Остановить">
        </div>
    </form>

    <iframe id="status" class="status" src="{{ action('Sitemap\SitemapController@generate') }}"></iframe>
@stop