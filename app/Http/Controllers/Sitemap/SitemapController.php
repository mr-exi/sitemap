<?php

namespace App\Http\Controllers\Sitemap;

use App\Sitemap;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Request;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SitemapController extends Controller
{

    public function index()
    {
        $paginationCount = 10;
        $pagination = Sitemap::paginate($paginationCount);
        $items = Sitemap::orderBy('created_at', 'desc')->paginate($paginationCount);

        return view('sitemap.list', compact('items', 'pagination'));
    }

    public function generate()
    {
        $sitemap = new Sitemap();

        try {
            if ($sitemap->init(Request::get('url'), Request::get('cache_id'))) {

                $started = false;

                while ($data = $sitemap->getNext()) {
                    if (!$started) {
                        echo view('sitemap.generate-start', compact('data'));
                        $started = true;
                    }
                    echo view('sitemap.generate', compact('data'));
                    @ob_flush();
                    flush();
                }

                return '';
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param integer $id
     * @return string
     */
    public function getFile($id = null)
    {
        try {

            $sitemap = Sitemap::findOrFail($id);

            $storageDisk = Storage::disk();

            $headers = [
                'Content-Type' => 'application/zip',
                'Content-Disposition' => 'attachment; filename="sitemap.zip"'
            ];

            $path = 'sitemap/saved/' . $sitemap->filename;

            if ($sitemap->filename && $storageDisk->exists($path)) {
                return Response($storageDisk->get($path), 200, $headers);
            } else {
                throw new FileNotFoundException('sitemap/saved/' . $sitemap->filename);
            }

        } catch (ModelNotFoundException $e) {
            throw new NotFoundHttpException();
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException();
        }
    }
}
