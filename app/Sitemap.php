<?php

namespace App;

use Cache;
use Curl\Curl;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use SimpleXMLElement;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use ZipArchive;

/**
 * Class Sitemap
 * @package App
 *
 * @property integer $id
 * @property string $name
 * @property string filename
 *
 */
class Sitemap extends Model
{
    protected $table = 'sitemaps';

    protected $fillable = ['name', 'filename'];

    static $allowedSchemes = ['http', 'https'],
        $reqUrlKeys = ['scheme', 'host', 'path', 'query', 'fragment'];

    private $step = 0,
        $stepDuration = 15,
        $allLinks = [],
        $activeLinks = [],
        $startTime = null,
        $startStepTime = null,

        $baseUrl = null,
        $scheme = 'http',
        $host = null,

        $wait = false,
        $redirect = false,

        $complete = false,
        $downloadUrl = '#',

        $cachedKeys = ['step', 'allLinks', 'activeLinks', 'startTime', 'baseUrl', 'scheme', 'host'],

        $storagePath = 'app/sitemap/saved/',
        $baseStorage = 'sitemap/saved/';


    /**
     *
     */
    private function saveToCache()
    {
        $data = [];

        foreach ($this->cachedKeys as $key) {
            $data[$key] = $this->{$key};
        }

        $cacheId = $this->getRandomHash();

        Cache::put($cacheId, $data, 1);

        $this->redirect = action('Sitemap\SitemapController@generate') . '?' . http_build_query(['url' => $this->baseUrl, 'cache_id' => $cacheId]);

    }

    private function getRandomHash()
    {
        return md5($this->baseUrl . rand(0, 1000) . microtime());
    }

    /**
     * @param null $cacheId
     * @return bool
     */
    private function getFromCache($cacheId = null)
    {
        $data = Cache::pull($cacheId);

        if (is_array($data)) {
            foreach ($this->cachedKeys as $key) {
                $this->{$key} = $data[$key];
            }
            return true;
        }

        return false;
    }

    /**
     * @param null $url
     * @param null $cacheId
     * @return bool
     * @throws \Exception
     */
    public function init($url = null, $cacheId = null)
    {
        if ($cacheId && $this->getFromCache($cacheId)) {
            return true;
        }

        $arUrl = $this->parseUrlFix(parse_url($url));

        if ($arUrl['scheme'] && $this->checkScheme($arUrl['scheme'])) {
            $this->scheme = $this->checkScheme($arUrl['scheme']);
        }

        $this->host = $arUrl['host'] ? $arUrl['host'] : $this->checkHost($url) ? $url : null;

        if ($this->scheme && $this->host) {

            $this->baseUrl = $this->normalizeUrl('/');

            $request = new Curl($this->baseUrl);
            $request->exec();

            switch ($request->httpStatusCode) {
                case 200:
                    $this->allLinks[] = $this->baseUrl = $this->normalizeUrl('/');
                    break;

                case 301:
                case 302:

                    $arNewUrl = $this->parseUrlFix(parse_url($request->responseHeaders['Location']));

                    if ($arNewUrl['host'] && $this->checkHost($arNewUrl['host'])) {
                        $this->host = $arNewUrl['host'];
                    }

                    if ($arNewUrl['scheme'] && $this->checkScheme($arNewUrl['scheme'])) {
                        $this->scheme = $arNewUrl['scheme'];
                    }

                    $this->allLinks[] = $this->baseUrl = $this->normalizeUrl('/');
                    break;

                default:
                    throw new \Exception('Ошибка запроса данных с сайта! Код ответа сервера ' . $request->httpStatusCode);
                    break;
            }

        } else {
            throw new \Exception($url ? 'Не удалось обработать url, введите его в формате http://site.ru/' : '');
        }

        return count($this->allLinks) > 0;
    }

    /**
     * @return array|bool
     */
    public function getNext()
    {
        if (!$this->baseUrl || $this->wait) {
            return false;
        }

        if (!$this->startTime) {
            $this->startTime = time();
        }

        if (!$this->startStepTime) {
            $this->startStepTime = time();
        }

        if ($this->step < count($this->allLinks)) {
            $stepUrl = $this->normalizeUrl(trim($this->allLinks[$this->step]));
            $request = new Curl($stepUrl);
            $request->exec();

            switch ($request->httpStatusCode) {
                case 200:
                    $dom = new \DOMDocument();
                    $dom->recover = true;
                    $dom->strictErrorChecking = false;

                    libxml_use_internal_errors(true);
                    $dom->loadHTML($request->response);
                    libxml_clear_errors();

                    $localBase = null;

                    foreach ($dom->getElementsByTagName('base') as $obBase) {
                        /**
                         * @var \DOMElement $obBase
                         */
                        $href = $obBase->getAttribute('href');
                        if ($this->checkUrl($href)) {
                            $localBase = $this->normalizeUrl($href);
                        }
                    }
                    unset($href);

                    foreach ($dom->getElementsByTagName('a') as $obLink) {
                        /**
                         * @var \DOMElement $obLink
                         */
                        $href = $this->normalizeUrl($obLink->getAttribute('href'), $localBase ? $localBase : $stepUrl);
                        if ($this->checkUrl($href) && !in_array($href, $this->allLinks)) {
                            $this->allLinks[] = $href;
                        }
                    }
                    unset($href);

                    $this->activeLinks[] = $this->allLinks[$this->step];

                    break;

                case 301:
                case 302:
                    $this->allLinks[] = $request->responseHeaders['Location'];
                    break;
            }

            $request->close();

            $this->step++;

            if (time() - $this->startStepTime > $this->stepDuration) {
                $this->wait = true;
                $this->saveToCache();
            } elseif ($this->step >= count($this->allLinks)) {
                $this->saveAsXml();
            }

        } else {
            return false;
        }

        return $this->getStatus();
    }

    /**
     * @param array $arUrl
     * @return array
     */
    private function parseUrlFix(array $arUrl = [])
    {
        foreach (self::$reqUrlKeys as $reqKey) {
            if (!array_key_exists($reqKey, $arUrl)) {
                $arUrl[$reqKey] = null;
            }
        }
        return $arUrl;
    }

    /**
     * @param null $url
     * @return bool
     */
    private function checkUrl($url = null)
    {
        $arUrl = $this->parseUrlFix(parse_url($url));
        return $this->checkScheme($arUrl['scheme']) && $this->checkHost($arUrl['host']) && $this->checkPath($arUrl['path']);
    }

    /**
     * @param null $scheme
     * @return bool
     */
    private function checkScheme($scheme = null)
    {
        return $scheme ? in_array($scheme, self::$allowedSchemes) : true;
    }

    /**
     * @param null $host
     * @return bool
     */
    private function checkHost($host = null)
    {
        $ret = $isCorrect = preg_match('/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/i', $host);

        if ($this->host) {
            $ret = $host ? $isCorrect && preg_replace('/^www./', '', $this->host) === preg_replace('/^www./', '', $host) : true;
        }

        return $ret;
    }

    /**
     * @param null $path
     * @return bool
     */
    private function checkPath($path = null)
    {
        return $path && preg_match('/(\.(aspx?|html?|jsp|php)|\/|^[^\.]+)$/i', $path);
    }

    /**
     * @param null $url
     * @param null $base
     * @return null|string
     */
    private function normalizeUrl($url = null, $base = null)
    {
        $ret = null;

        if ($url && $this->checkUrl($url)) {

            $arUrl = $this->parseUrlFix(parse_url($url));

            if (preg_match('/^\//', $arUrl['path'])) {
                $ret = $this->scheme . '://' . $this->host . $arUrl['path'];
            } elseif ($arBase = parse_url($this->normalizeUrl($base))) {
                $base = preg_replace('/\/$/', '', $arBase['scheme'] . '://' . $arBase['host'] . $arBase['path']);
                $ret = $base . '/' . $arUrl['path'];
            }

            if ($arUrl['query']) {
                $ret .= '?' . $arUrl['query'];
            }

        }

        return $ret;
    }

    /**
     * @return array
     */
    private function getStatus()
    {
        $allLinksCount = count($this->allLinks);

        $timePassed = time() - $this->startTime;
        $speed = $this->step / ($timePassed > 0 ? $timePassed : 1);

        $timeLeft = ceil((float)(($allLinksCount - $this->step) / $speed));


        return [
            'complete' => $this->complete,

            'wait' => $this->wait,
            'redirect' => $this->redirect,

            'current' => $this->allLinks[$this->step - 1],
            'scanned' => $this->step,
            'added' => count($this->activeLinks),
            'left' => $allLinksCount - $this->step,
            'time' => [
                'passed' => $timePassed,
                'passedFormat' => date('i:s', mktime(0, 0, $timePassed)),
                'left' => $timeLeft,
                'leftFormat' => date('i:s', mktime(0, 0, $timeLeft))
            ],
            'downloadUrl' => $this->downloadUrl
        ];
    }

    private function getRandomPath()
    {
        $name = $this->getRandomHash();
        return file_exists(storage_path($this->storagePath . $name . '.zip')) ? $this->getRandomPath() : storage_path($this->storagePath . $name . '.zip');
    }

    /**
     *
     */
    private function saveAsXml()
    {

        $storageDisk = Storage::disk();

        $xmlTemplate = $storageDisk->get('sitemap/template.xml');

        $obXml = new SimpleXMLElement($xmlTemplate);

        foreach ($this->activeLinks as $url) {
            $obXml->addChild('url')->addChild('loc', $url);
        }
        $zipPath = $this->getRandomPath();

        $zip = new ZipArchive();
        try {

            if ($zip->open($zipPath, ZipArchive::CREATE) !== true) {
                throw new FileException('Невозможно создать zip-архив!');
            }
            if (!$zip->addFromString('sitemap.xml', $obXml->asXML())) {
                throw new FileException('Невозможно добавить данные к архиву ' . $zipPath);
            }

            $this->name = $this->baseUrl;
            $this->filename = basename($zipPath);


            if ($this->Save()) {

                $this->complete = true;

                $id = $this->id;
                $this->downloadUrl = action('Sitemap\SitemapController@getFile', compact('id'));

            }

        } catch (FileException $e) {
            echo $e->getMessage();
            $zip->close();
            unlink($zipPath);

        } catch (QueryException $e) {
            echo $e->getMessage();
            unlink($zipPath);

        }
    }
}
